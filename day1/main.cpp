#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

int main() {

    std::ifstream inFile; // our file
    inFile.open("../codes.txt"); // the codes for our freq
    std::string freq; // store our freq
    int finalFreq = 0;
    std::vector<int> myValues, codes;

    if (!inFile.is_open())
        return 1; // we ran into an error!

    bool foundVal = false;

    while (!inFile.eof())
    {
        std::getline(inFile, freq);
        codes.push_back(std::stoi(freq.substr(1)) * ((freq.substr(0,1) == "-") ? -1 : 1));
    }

    while (!foundVal) {
        for (int item : codes)
        {
            finalFreq += item;
            //myValues.push_back(finalFreq += );
            if (std::find(myValues.begin(), myValues.end(), finalFreq) != myValues.end())
            {
                std::cout << "Found a match: " << finalFreq << std::endl;
                foundVal = true;
            }

            myValues.push_back(finalFreq);
        }
        std::cout << "Final Frequency: " << finalFreq << std::endl;
    }

    return 0;
}