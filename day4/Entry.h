//
// Created by taco on 12/5/18.
//

#ifndef DAY4_ENTRY_H
#define DAY4_ENTRY_H

#include <regex>
#include <string>
#include <fstream>

// [1518-07-18 00:55] falls asleep

class Entry
{
public:
    Entry() {year = month = day = hour = minute = 0;};
    Entry(std::string);

    int getYear() {return year;};
    int getMonth() {return month;};
    int getDay() {return day;};
    int getHour() {return hour;};
    int getMinute() {return minute;};
    std::string getData() {return data;};
    std::string getOriginal() {return original;};

    void setYear(int newYear) {year = newYear;};
    void setMonth(int newMonth) {month = newMonth;};
    void setDay(int newDay) {day = newDay;};
    void setHour(int newHour) {hour = newHour;};
    void setMinute(int newMinute) {minute = newMinute;};

    void loadString(std::string input);

    bool operator==(const Entry &other);
    bool operator!=(const Entry &other);
    bool operator>(const Entry &other);
    bool operator<(const Entry &other);
    bool operator>=(const Entry &other);
    bool operator<=(const Entry &other);

    friend int operator-(const Entry &e1, const Entry &e2);

    friend std::ifstream& operator>>(std::ifstream &stream, Entry &e);
private:
    int year;
    int month;
    int day;
    int hour;
    int minute;

    std::string original;
    std::string data;
};


#endif //DAY4_ENTRY_H
