#include <iostream>
#include <regex>

#include "Entry.h"
#include "btree/Tree.h"

using TreeForest::Tree;

struct Guard {
    int id;
    int timeslept;
    int hours[60];
};

int findGuard(int id, std::vector<Guard> &guards);
int findLargest(int arr[60]);
void loadEntries(std::string file, Tree<Entry> &entries);
void printEntries(Entry &entry, int count);

int main()
{
    std::ifstream inFile;
    Tree<Entry> Entries;

    loadEntries("../input.txt", Entries);

    Entry a("[1518-11-11 00:16] falls asleep");
    Entry b("[1518-11-11 00:39] wakes up");

    std::cout << b-a;

    std::vector<Guard> myGuards;
    std::vector<Entry> myEntries = Entries.flatten();

    int onShift = -1;
    Entry fellAsleep;
    bool asleep = false;

    for (int i = 0; i < myEntries.size(); i++)
    {
        std::smatch store; // Guard #2677 begins shift
        std::string regStr = R"(Guard #(\d+) begins shift)";
        std::string data = myEntries[i].getData();

        if (std::regex_search(data, store, std::regex(regStr)))
        {
            onShift = std::stoi(store[1].str());
        }
        else if ( data == "falls asleep")
        {
            fellAsleep = myEntries[i];
            asleep = true;
        }
        else if ( data == "wakes up" )
        {
            int newVal = myEntries[i] - fellAsleep - 1;
            int find = findGuard(onShift, myGuards);

            if (find != -1)
            {
                myGuards[find].timeslept += newVal;
                for (int j = fellAsleep.getMinute(); j<60 && j < myEntries[i].getMinute(); j++)
                {
                    myGuards[find].hours[j]++;
                }
            }
            else
            {
                Guard newGuard{};
                newGuard.id = onShift;
                newGuard.timeslept = newVal;
                for (int j = fellAsleep.getMinute(); j<60 && j < myEntries[i].getMinute(); j++)
                {
                    newGuard.hours[j]++;
                }

                myGuards.push_back(newGuard);
            }
            asleep = false;
        }
    }

    Guard largeGuard = myGuards[0];
    int largestHour = 0;
    for (auto guard : myGuards)
    {
        int newHour = findLargest(guard.hours);

        if (guard.hours[newHour] > largeGuard.hours[largestHour])
        {
            largestHour = newHour;
            largeGuard = guard;
        }
    }

    std::cout << "And the guard value is: " << largeGuard.id * largestHour << std::endl;

    return 0;
}

void loadEntries(std::string file, Tree<Entry> &entries)
{
    std::ifstream inFile;

    inFile.open("../input.txt");

    if (!inFile.is_open())
        return;

    while (!inFile.eof())
    {
        Entry tmp;
        inFile>>tmp;
        entries.add(tmp);
    }

    //entries.inOrderTraverse(printEntries);
}

int findGuard(int id, std::vector<Guard> &guards)
{
    for (int i = 0; i < guards.size(); i++)
    {
        if (guards[i].id == id)
        {
            return i;
        }
    }
    return -1;
}

int findLargest(int arr[60])
{
    int largest = 0;
    for (int i = 0; i < 60; i++)
    {
        if (arr[i] > arr[largest])
            largest = i;
    }
    return largest;
}

void printEntries(Entry &entry, int count)
{
    std::cout << entry.getOriginal() << std::endl;
}