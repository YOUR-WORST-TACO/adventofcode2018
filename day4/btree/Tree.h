//
// Created by taco on 11/14/18.
//

/*
 * 1. pre-order - process root, left, then right:
 *      write, copy assignemnt
 *
 * 2. in-order - process the left subtree, root, then right:
 *      print ascending
 *
 * 3. post-order - process the left, right, then root:
 *      Free
 *
 * 4. backward in order - process right, root, then left:
 *      print backwards
 */

#ifndef PROJECT5_TREE_H
#define PROJECT5_TREE_H

#include <iostream>
#include <fstream>
#include <vector>
#include "Branch.h"

namespace TreeForest
{
    enum {
        ASCENDING,
        DESCENDING
    };

    template<class Type>
    class Tree
    {
    public:
        Tree();
        Tree(const Tree<Type> &copy);
        ~Tree();
        void add(Type dataIn);
        void remove(Type dataIn);
        size_t size() {return size(root);};
        void print() {print(root);}
        void write(std::string file);
        void read(std::string file);
        void traverse(void process(Type &, int), int mode);
        void inOrderTraverse(void process(Type &, int));
        void revOrderTraverse(void process(Type &, int));

        std::vector<Type> flatten();

        Tree<Type> &operator=(const Tree<Type> &other);

    private:
        Branch<Type>* root;
        void copyTree(Branch<Type>* otherCursor);
        void free(Branch<Type>* cursor);
        size_t size(Branch<Type>* cursor);
        void print(Branch<Type>* cursor);
        void write(Branch<Type>* cursor, std::ofstream &stream);
        Branch<Type> *AllocBinNode(Type dataIn);
        void traverse(Branch<Type> *cursor, int &, void process(Type &, int), int mode);

        void flatten(Branch<Type>* cursor, std::vector<Type> &array);
    };
}

template<class Type>
using BinTree = TreeForest::Tree<Type>;

#include "Tree.tpp"

#endif //PROJECT5_TREE_H
