//
// Created by taco on 11/14/18.
//

namespace TreeForest
{
    template<class Type>
    Tree<Type>::Tree()
    {
        root = nullptr;
    }

    template<class Type>
    Tree<Type>::Tree(const Tree<Type> &copy)
    {
        root = nullptr;

        copyTree(copy.root);
    }

    template<class Type>
    Tree<Type>::~Tree()
    {
        free(root);
        root = nullptr;
    }

    template<class Type>
    void Tree<Type>::add(Type dataIn)
    {
        Branch<Type>* cursor = root;
        Branch<Type>* allocNode = AllocBinNode(dataIn);

        if(root == nullptr)
        {
            root = allocNode;
            return;
        }
        while(cursor != nullptr)
        {
            if(dataIn <= cursor->data)    // left <=
            {
                if(cursor->left == nullptr)
                {
                    cursor->left = allocNode;
                    cursor = nullptr;
                }
                else
                    cursor = cursor->left;
            }
            else                        // right >
            {
                if(cursor->right == nullptr)
                {
                    cursor->right = allocNode;
                    cursor = nullptr;
                }
                else
                    cursor = cursor->right;
            }
        }
    }

    template<class Type>
    void Tree<Type>::remove(Type dataIn)
    {

        Branch<Type> *cursor = nullptr;
        Branch<Type> *tmpPtr = nullptr;
        Branch<Type> *subCursor= nullptr;


        if (root != nullptr && root->data == dataIn) // check our root node
        { // if our root node data is dataIn
            tmpPtr = root;
            if (tmpPtr->left != nullptr)
            {
                root = tmpPtr->left; // set root to root->left

                subCursor = root;

                while (subCursor->right != nullptr)
                { // find the last right node from our new root
                    subCursor = subCursor->right;
                }

                // put our old root right onto new root far right
                subCursor->right = tmpPtr->right;
            }
            else
            {
                root = tmpPtr->right;
            }

            delete tmpPtr; // delete old root
        }

        cursor = root;

        while (cursor != nullptr) // while we have a cursor
        {
            if (dataIn <= cursor->data) // check the things on the left
            {
                if (cursor->left != nullptr && cursor->left->data == dataIn)
                {
                    tmpPtr = cursor->left;
                    cursor->left = tmpPtr->left;

                    subCursor = cursor->left;

                    if (subCursor != nullptr)
                    {
                        while (subCursor->right != nullptr)
                        {
                            subCursor = subCursor->right;
                        }

                        subCursor->right = tmpPtr->right;
                    }
                    else
                    {
                        cursor->left = tmpPtr->right;
                    }

                    delete tmpPtr;
                }
                else
                {
                    cursor = cursor->left;
                }
            }
            else // check the things on the right
            {
                if (cursor->right != nullptr && cursor->right->data == dataIn)
                { // if the thing on the right is equal to our data

                    tmpPtr = cursor->right;
                    cursor->right = tmpPtr->left;

                    subCursor = cursor->right;

                    if (subCursor != nullptr)
                    {
                        while (subCursor->right != nullptr)
                        {
                            subCursor = subCursor->right;
                        }

                        subCursor->right = tmpPtr->right;
                    }
                    else
                    {
                        cursor->right = tmpPtr->right;
                    }


                    delete tmpPtr;
                }
                else // if it is not equal
                {
                    cursor = cursor->right;
                }
            }
        }
    }

    template<class Type>
    void Tree<Type>::write(std::string file)
    {
        std::ofstream outFile;
        outFile.open(file.c_str());

        write(root, outFile);
    }

    template<class Type>
    void Tree<Type>::read(std::string file)
    {
        std::ifstream inFile;
        inFile.open(file.c_str());
        Type data;

        if (!inFile.is_open())
        {
            return;
        }

        free(root);
        root = nullptr;

        inFile >> data;
        while (!inFile.eof())
        {
            add(data);
            inFile >> data;
        }
    }

    template<class Type>
    void Tree<Type>::traverse(void process(Type &, int), int mode)
    {
        int count = 1;
        traverse(root, count, process, mode);
    }

    template<class Type>
    void Tree<Type>::inOrderTraverse(void process(Type &, int))
    {
        int count = 1;
        traverse(root, count, process, ASCENDING);
    }

    template<class Type>
    void Tree<Type>::revOrderTraverse(void process(Type &, int))
    {
        int count = 1;
        traverse(root, count, process, DESCENDING);
    }

    template<class Type>
    std::vector<Type> Tree<Type>::flatten()
    {
        std::vector<Type> myVector;
        flatten(root, myVector);
        return myVector;
    }

    template<class Type>
    void Tree<Type>::copyTree(Branch<Type> *otherCursor)
    {
        if (otherCursor != nullptr)
        {
            add(otherCursor->data);
            copyTree(otherCursor->left);
            copyTree(otherCursor->right);
        }
    }

    template<class Type>
    void Tree<Type>::free(Branch<Type> *cursor)
    {
        if(cursor != nullptr)  // Post-Order Traverse - stopping case
        {
            free(cursor->left);
            free(cursor->right);
            delete cursor;
        }
    }

    template<class Type>
    size_t Tree<Type>::size(Branch<Type>* cursor)
    {
        if (cursor != nullptr)
        {
            return 1 + size(cursor->left) + size(cursor->right);
        }
        return 0;
    }

    template<class Type>
    void Tree<Type>::print(Branch<Type> *cursor)
    {
        if(cursor != nullptr)  // In Order Traverse - stopping case
        {
            print(cursor->left);
            std::cout << cursor->data << std::endl;
            print(cursor->right);
        }
    }

    template<class Type>
    void Tree<Type>::write(Branch<Type> *cursor, std::ofstream &stream)
    {
        if (cursor != nullptr)
        {
            stream << cursor->data << std::endl;
            write(cursor->left, stream);
            write(cursor->right, stream);
        }
    }

    template<class Type>
    void Tree<Type>::traverse(Branch<Type> *cursor, int &count, void process(Type &, int), int mode)
    {
        if (cursor != nullptr)
        {
            switch(mode)
            {
                case ASCENDING:
                    traverse(cursor->left, count, process, mode);
                    process(cursor->data, count);
                    count++;
                    traverse(cursor->right, count, process, mode);
                    break;
                case DESCENDING:
                    traverse(cursor->right, count, process, mode);
                    process(cursor->data, count);
                    count++;
                    traverse(cursor->left, count, process, mode);
                    break;
                default:
                    traverse(cursor, count, process, ASCENDING);
                    break;
            }
        }
    }

    template<class Type>
    Branch<Type> *Tree<Type>::AllocBinNode(Type dataIn)
    {
        return (new Branch<Type>(dataIn));
    }

    template<class Type>
    Tree<Type> &Tree<Type>::operator=(const Tree<Type> &other)
    {
        if (this == &other)
        {
            return *this;
        }

        free(root);

        copyTree(other.root);
    }

    template<class Type>
    void Tree<Type>::flatten(Branch<Type>* cursor, std::vector<Type> &array)
    {
        if (cursor != nullptr)
        {
            flatten(cursor->left, array);
            array.push_back(cursor->data);
            flatten(cursor->right, array);
        }
    }
}