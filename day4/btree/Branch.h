/*
 * Branch.h
 * Programmer: Stephen Tafoya
 * 2018
 *
 * Contains declaration of class Branch in TreeForest for binary tree
 * Members of Branch
 *      Default constructor
 *      Copy constructor
 *      Left and Right branch pointers
 *      Template Data
 *
 * Note: Branch is a friend of Tree
 */

#ifndef PROJECT5_BRANCH_H
#define PROJECT5_BRANCH_H

namespace TreeForest            // This is the namespace for Branch and Tree
{
    template<class Type>                      // Declare our template for the class
    class Branch
    {
    private:
        Branch();               // default constructor
        Branch(Type dataIn);    // copy constructor
        Branch *left, *right;   // pointers to left and right Branches
        Type data;              // Template Data

        template<class FriendType>            // define template FriendType for Friend
        friend class Tree;      // declare tree is a friend of Branch
                                // this makes our private members available to Tree
    };
}

#include "Branch.tpp"           // include the template file

#endif //PROJECT5_BRANCH_H
