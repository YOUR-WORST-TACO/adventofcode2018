/*
 * Branch.cpp
 * Programmer: Stephen Tafoya
 * 2018
 *
 * Contains definitions of Branch.h class Branch
 * the following are defined in this file
 *      Branch() default constructor
 *      Branch(Type dataIn) copy constructor
 */

namespace TreeForest                    // namespace for Tree and Branch
{
    /* Default constructor
     *      sets the left and right pointer to nullptr
     */
    template<class Type>
    Branch<Type>::Branch()
    {
        right = left = nullptr;
    }

    /* Assignment constructor
     *      assigns the data for Branch
     */
    template<class Type>
    Branch<Type>::Branch(Type dataIn)
    {
        right = left = nullptr;
        data = dataIn;                  // make sure to define data as dataIn
    }
}