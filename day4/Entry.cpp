//
// Created by taco on 12/5/18.
//

#include "Entry.h"

Entry::Entry(std::string input)
{
    loadString(input);
}

void Entry::loadString(std::string input)
{
    std::smatch store;
    std::string reg = R"(\[(\d+)-(\d+)-(\d+) (\d+):(\d+)] (.*))";
    if (std::regex_search(input, store, std::regex(reg)))
    {
        original = input;

        year = std::stoi(store[1].str());
        month = std::stoi(store[2].str());
        day = std::stoi(store[3].str());
        hour = std::stoi(store[4].str());
        minute = std::stoi(store[5].str());

        data = store[6].str();
    }
}

bool Entry::operator==(const Entry &other)
{
    if (this->year == other.year)
    {
        if (this->month == other.month)
        {
            if (this->day == other.day)
            {
                if (this->hour == other.hour)
                {
                    if (this->minute == other.minute)
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}

bool Entry::operator!=(const Entry &other)
{
    return !(*this == other);
}

bool Entry::operator>(const Entry &other)
{
    if (this->year > other.year) // check if year1 is bigger than year2
    {
        return true;
    }
    else if (this->year == other.year)
    {
        if (this->month > other.month) // check if month1 is bigger
        {
            return true;
        }
        else if (this->month == other.month)
        {
            if (this->day > other.day) // check if day1 is bigger
            {
                return true;
            }
            else if (this->day == other.day)
            {
                if (this->hour > other.hour) // check if hour is bigger
                {
                    return true;
                }
                else if (this->hour == other.hour)
                {
                    if (this->minute > other.minute) // check if minute is bigger
                    {
                        return true;
                    }
                }
            }
        }
    }
//    if (this->year >= other.year)
//    {
//        if (this->month >= other.month)
//        {
//            if (this->day >= other.day)
//            {
//                if (this->hour >= other.hour)
//                {
//                    if (this->minute > other.minute)
//                    {
//                        return true;
//                    }
//                }
//            }
//        }
//    }
    return false;
}

bool Entry::operator<(const Entry &other)
{
    return !(*this > other) && (*this != other);
}

bool Entry::operator>=(const Entry &other)
{
    return (*this > other) || (*this == other);
}

bool Entry::operator<=(const Entry &other)
{
    return (*this < other) || (*this == other);
}

int operator-(const Entry &e1, const Entry &e2)
{
    int difference = 0;
    difference += (e1.day - e2.day)*24*60;
    difference += (e1.hour - e2.hour)*60;
    difference += (e1.minute - e2.minute);

    return difference;
}

std::ifstream& operator>>(std::ifstream &stream, Entry &e)
{
    std::string input;
    getline(stream, input);

    e.loadString(input);

    return stream;
}