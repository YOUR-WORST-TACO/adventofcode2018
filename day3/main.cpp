#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <vector>
#include "cut.h"

void makeCut(int x, int y, int w, int h, int sheet[1000][1000]);

int main()
{
    int sheet[1000][1000];
    std::vector<Cut> cuts;
    int x,y,w,h;
    int overlap = 0;
    std::string input;
    std::smatch matches;
    std::regex sheetInput (R"(#(\d+) @ (\d+),(\d+): (\d+)x(\d+))");
    std::ifstream inFile;

    inFile.open("../input.txt");

    if (!inFile.is_open())
        return 1;

    for (auto &i : sheet)
        for (auto &j : i)
            j = 0;

    while (!inFile.eof())
    {
        getline(inFile, input);
        if (std::regex_search(input, matches, sheetInput) && matches.size() == 6)
        {
            Cut tmp;
            tmp.id = std::stoi(matches[1].str());
            tmp.x = std::stoi(matches[2].str());
            tmp.y = std::stoi(matches[3].str());
            tmp.w = std::stoi(matches[4].str());
            tmp.h = std::stoi(matches[5].str());

            cuts.push_back(tmp);

            makeCut(tmp.x, tmp.y, tmp.w, tmp.h, sheet);
        }
    }

    for (auto &i : sheet)
    {
        for (auto &j : i)
        {
            if (j > 1)
            {
                overlap++;
            }
        }
    }

    std::cout << "Overlaps: " << overlap << std::endl;

    for (int i = 0; i < cuts.size(); i++)
    {
        bool clean = true;
        for (int _x = cuts[i].x; _x < 1000 && _x < cuts[i].x + cuts[i].w; _x++)
        {
            for (int _y = cuts[i].y; _y < 1000 && _y < cuts[i].y + cuts[i].h; _y++)
            {
                if (sheet[_x][_y] == 0 || sheet[_x][_y] > 1)
                {
                    clean = false;
                }
            }
        }

        if (clean)
        {
            std::cout << "cut id: " << cuts[i].id;
        }
    }

    return 0;
}

void makeCut(int x, int y, int w, int h, int sheet[1000][1000])
{
    for (int i = x; i < 1000 && i < x + w; i++)
    {
        for (int j = y; j < 1000 && j < y + h; j++)
        {
            sheet[i][j]++;
        }
    }
}