#include <iostream>
#include <fstream>
#include <vector>
#include <string>

int countChars(char c, std::string source);
int compareStrings(std::string left, std::string right);

int main()
{
    int twoLetter = 0, threeLetter = 0;
    std::vector<std::string> input;

    std::string placeHold;

    std::string test1 = "abcdaefg", test2 = "aabbcccd", test3;

    std::ifstream inFile;
    inFile.open("../input.txt");

    if (!inFile.is_open())
        return 1;

    while (!inFile.eof())
    {
        std::getline(inFile, placeHold);
        input.push_back(placeHold);
    }

    for (int i = 0; i < input.size(); i++)
    {
        for (int j = i+1; j < input.size(); j++)
        {
            int pos = -1;
            if ((pos = compareStrings(input[i], input[j])) > 0)
            {
                std::cout << "Found similar: " << input[i] << " at pos " << pos << std::endl;
            }
        }
    }

    std::cout << "Checksum is: " << twoLetter*threeLetter << std::endl;

    return 0;
}

int countChars( char c, std::string source)
{
    int count = 0;
    for (char _c : source)
    {
        if (_c == c)
            count++;
    }
    return count;
}

int compareStrings(std::string left, std::string right)
{
    bool twoNotSimilar = false;
    int simPlace = -1;
    for (int i = 0; i < left.size(); i++)
    {
        if (left[i] != right[i])
        {
            if (!twoNotSimilar)
            {
                simPlace = i;
                twoNotSimilar = true;
            }
            else
            {
                return -1;
            }
        }
    }
    return simPlace;
}