#include <iostream>
#include <fstream>
#include <string>

std::string react(std::string basePoly);

int main()
{
    std::ifstream inFile;
    inFile.open("../input.txt");
    std::string myShit, saveShit;

    unsigned long largest = 10000000;
    
    char alphab[26] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

    if (!inFile.is_open())
        return 1;

    std::getline(inFile, myShit);

    saveShit = myShit;

    for (int i = 0; i < 26; i++)
    {
        for (int j = 0; j < myShit.size(); j++)
        {
            if (myShit[j] == alphab[i] || myShit[j] == toupper(alphab[i]))
            {
                myShit.erase(j, 1);
                j--;
            }
        }

        std::cout << "Working on set: " << alphab[i] << "\nSet: " << myShit
                    << "\n\toriginal size: " << saveShit.size()
                    << "\n\tcleaned size: " << myShit.size() << "\n\n";

        myShit = react(myShit);

        if (myShit.size() < largest)
            largest = myShit.size();

        myShit = saveShit;
    }

    //std::cout << myShit << std::endl << "Length: " << myShit.size() << std::endl;

    std::cout << "Shortest: " << largest;

    return 0;
}

std::string react(std::string basePoly)
{
    bool clean = false;

    while (!clean)
    {
        clean = true;
        for (int i = 0; i < basePoly.size() - 1; i++)
        {
            if (basePoly[i] == toupper(basePoly[i]) && tolower(basePoly[i]) == basePoly[i + 1])
            {
                //std::cout << "Erasing: " << basePoly[i] << basePoly[i + 1] << std::endl;
                basePoly.erase(i, 2);
                clean = false;
            }
            else if (basePoly[i] == tolower(basePoly[i]) && toupper(basePoly[i]) == basePoly[i + 1])
            {
                //std::cout << "Erasing: " << basePoly[i] << basePoly[i + 1] << std::endl;
                basePoly.erase(i, 2);
                clean = false;
            }
        }
    }
    return basePoly;
}